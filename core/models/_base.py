from typing import Any

import sqlalchemy as sa
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.ext.declarative import as_declarative, declared_attr

from core.services import get_db


@as_declarative()
class BaseModel:
    id: Any
    __name__: str

    # Generate __tablename__ automatically
    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()

    def to_dict(self):
        return {k: self.__dict__[k] for k in self.__dict__ if not k.startswith('_')}


@as_declarative()
class IntegerIdMixin:
    id = sa.Column(INTEGER(25), primary_key=True)


@as_declarative()
class TimestampMixin:
    created_at = sa.Column(sa.DateTime, comment='创建时间')
    updated_at = sa.Column(sa.DateTime, comment='修改时间')
    deleted_at = sa.Column(sa.DateTime, comment='删除时间')
    created_by = sa.Column(sa.INTEGER, comment='创建者')
    updated_by = sa.Column(sa.INTEGER, comment='修改者')
    deleted_by = sa.Column(sa.INTEGER, comment='删除者')
