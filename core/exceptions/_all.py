from starlette.exceptions import HTTPException
import typing as tp


class UnicornException(Exception):
    def __init__(self, name: str):
        self.name = name


class MyHTTPException(HTTPException):
    title: str

    def __init__(self, title, detail: tp.Any = None, status_code: int = 4000):
        if detail is None:
            detail = ''
        super().__init__(status_code, detail)
        self.title = title
