from datetime import datetime

import arrow
import pendulum
from pydantic import BaseModel

from core import formatters


class BaseTransformer(BaseModel):
    pass


class OrmTransformer(BaseModel):
    id: int

    class Config:
        orm_mode = True

        json_encoders = {
            pendulum.pendulum.Pendulum: lambda t: t.to_datetime_string(),
            datetime: formatters.format_datetime,
            # arrow.Arrow: lambda t: t.format('YYYY-MM-DD HH:mm:ss'),
        }


class OrmTransformerWithTimestamp(OrmTransformer):
    created_at: datetime
    updated_at: datetime
    # deleted_at: datetime = None
    created_by: int
    updated_by: int
    # deleted_by: int = None
