import typing as tp

from pathlib import Path
from importlib import import_module

from core.resources._base import BaseResource

AVAILABLE_RESOURCES: tp.List[BaseResource] = []

from core.configs import app as APP

for m in APP.ENABLE_MODULES:
    for f in (APP.PROJECT_PATH / m / 'resources').glob('*.py'):
        if not f.name.startswith('_') and f.is_file():
            module = import_module(f'.{f.stem}', f'{m}.resources')

            try:
                AVAILABLE_RESOURCES.append(module.resource)
            except:  # noqa: E722 do not use bare 'except'
                pass


# from core.resources.address import AddressResource
#
# AVAILABLE_RESOURCES = [
#     AddressResource,
# ]


def register_resources(app):
    for resource in AVAILABLE_RESOURCES:
        app.include_router(resource.router)
