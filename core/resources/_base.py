import typing as tp

from fastapi import APIRouter

from core.actions._base import ListAction, BaseAction, DetailAction, CreateAction, PartialUpdateAction, DeleteAction
from core.repositories._base import BaseRepository
from core.transformers._base import BaseTransformer, OrmTransformer
from core.validators._base import BaseValidator


class BaseResource():
    name: str
    name_doc: str
    path: str
    router: APIRouter = APIRouter()
    Actions: tp.List[tp.Type[BaseAction]] = []

    repository: BaseRepository
    DetailTransformer: tp.Type[BaseTransformer]

    def __init__(self):
        self.router: APIRouter = APIRouter(prefix=self.path, tags=[self.name_doc])

    def register_resource(self):
        for Action in self.Actions:
            Transformer_ = Action.Transformer if Action.Transformer is not None else self.DetailTransformer
            Action(self.repository, Transformer_).register_action(self.router)

        return self


class CRUDResource(BaseResource):
    DetailTransformer: tp.Type[OrmTransformer]
    SummaryTransformer: tp.Type[OrmTransformer] = None

    create_Validator: BaseValidator = None
    partial_update_Validator: BaseValidator = None

    enable_list: bool = True
    enable_create: bool = True
    enable_detail: bool = True
    enable_partial_update: bool = True
    enable_delete: bool = True

    def register_resource(self):
        if self.enable_list:
            the_transformer_class = self.DetailTransformer
            if self.SummaryTransformer: the_transformer_class = self.SummaryTransformer
            (ListAction(self.repository, the_transformer_class).register_action(self.router))

        if self.enable_create:
            (CreateAction(self.repository, self.DetailTransformer, Validator=self.create_Validator)
             .register_action(self.router))

        if self.enable_detail:
            (DetailAction(self.repository, self.DetailTransformer)
             .register_action(self.router))

        if self.enable_partial_update:
            (PartialUpdateAction(self.repository, self.DetailTransformer, Validator=self.partial_update_Validator)
             .register_action(self.router))

        if self.enable_delete:
            (DeleteAction(self.repository, self.DetailTransformer)
             .register_action(self.router))

        return super().register_resource()
