from core.resources._base import CRUDResource

from eve.models.virtual_macihne_template import VirtualMachineTemplate as Model
from eve.repositories.virtual_machine_template import VirtualMachineTemplateRepository as Repository
from eve.transformers.virtual_machine_template import (
    VirtualMachineTemplateDetail as DetailTransformer,
    VirtualMachineTemplateSummary as SummaryTransformer,
)


class VirtualMachineTemplateResource(CRUDResource):
    name = 'virtual_machine_template'
    name_doc = '虚拟机模板'
    path = '/virtual_machine_templates'

    repository = Repository(Model)
    DetailTransformer = DetailTransformer
    SummaryTransformer = SummaryTransformer

    # create_Validator = Validator1
    # partial_update_Validator = Validator2

    enable_create: bool = False
    enable_partial_update: bool = False
    enable_delete: bool = False


resource = VirtualMachineTemplateResource().register_resource()
