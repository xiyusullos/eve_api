from core.resources._base import CRUDResource
from eve.models.virtual_machine_interface import VirtualMachineInterface
from eve.repositories.virtual_machine_interface import VirtualMachineInterfaceRepository
from eve.transformers.virtual_machine_interface import VirtualMachineInterface as Transformer
from eve.validators.virtual_machine_interface import (
    VirtualMachineInterfaceCreatingValidator as Validator1,
    VirtualMachineInterfacePartialUpdatingValidator as Validator2,
)


class VirtualMachineInterfaceResource(CRUDResource):
    name = 'virtual_machine_interface'
    name_doc = '虚拟机网卡'
    path = '/virtual_machine_interfaces'

    repository = VirtualMachineInterfaceRepository(VirtualMachineInterface)
    DetailTransformer = Transformer

    create_Validator = Validator1
    partial_update_Validator = Validator2


resource = VirtualMachineInterfaceResource().register_resource()
