from core.resources._base import CRUDResource
from eve.actions.virtual_machine_instance import CreateVirtualMachineAction, BootDomainAction, DestroyDomainAction, \
    ShutdownDomainAction, CheckStatusAction, DetailConsoleAction, InstantiateVirtualMachineAction
from eve.models.virtual_machine_instance import VirtualMachineInstance as Model
from eve.repositories.virtual_machine_instance import VirtualMachineInstanceRepository as Repository
from eve.transformers.virtual_machine_instance import (
    VirtualMachineInstanceDetail as DetailTransformer,
    VirtualMachineInstanceSummary as SummaryTransformer,
)
from eve.validators.virtual_machine_instance import (
    VirtualMachineInstanceCreatingValidator as Validator1,
    VirtualMachineInstancePartialUpdatingValidator as Validator2
)


class VirtualMachineInstanceResource(CRUDResource):
    name = 'virtual_machine_instance'
    name_doc = '虚拟机实例'
    path = '/virtual_machine_instances'

    repository = Repository(Model)
    DetailTransformer = DetailTransformer
    SummaryTransformer = SummaryTransformer

    Actions = [
        BootDomainAction,
        # DestroyDomainAction,
        ShutdownDomainAction,
        DetailConsoleAction,
        # CheckStatusAction,
        InstantiateVirtualMachineAction,

    ]
    enable_delete = False

    create_Validator = Validator1
    partial_update_Validator = Validator2


resource = VirtualMachineInstanceResource().register_resource()
