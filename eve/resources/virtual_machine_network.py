from core.resources._base import CRUDResource
from eve.models.virtual_machine_network import VirtualMachineNetwork as Model
from eve.repositories.virtual_machine_network import VirtualMachineNetworkRepository as Repository
from eve.transformers.virtual_machine_network import (
    VirtualMachineNetworkDetail as DetailTransformer,
    VirtualMachineNetworkSummary as SummaryTransformer,
)
from eve.validators.virtual_machine_network import (
    VirtualMachineNetworkCreatingValidator as CreatingValidator,
    VirtualMachineNetworkPartialUpdatingValidator as PartialUpdatingValidator
)


class VirtualMachineNetworkResource(CRUDResource):
    name = 'virtual_machine_network'
    name_doc = '虚拟机网络'
    path = '/virtual_machine_networks'

    repository = Repository(Model)
    DetailTransformer = DetailTransformer
    SummaryTransformer = SummaryTransformer

    create_Validator = CreatingValidator
    partial_update_Validator = PartialUpdatingValidator


# resource = VirtualMachineNetworkResource().register_resource()
