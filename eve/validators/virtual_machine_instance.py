from tkinter.scrolledtext import example
from uuid import uuid4

from pydantic import Field

from core.validators._base import BaseValidator
from eve.models.virtual_machine_instance import LibvirtDomain, VirtualMachineInstance


class LibvirtDomainCreatingValidator(BaseValidator):
    memory: int = Field(example=256, ge=1)
    cpu_num: int = Field(example=1, ge=1)
    os_arch_type: LibvirtDomain.OS_ARCH_TYPE = Field(title='OS架构类型', description='1-x86_64；')


class VirtualMachineInstanceCreatingValidator(BaseValidator):
    virtual_machine_template_id: int = Field(title='虚拟机模板id', example=0)
    libvirt_domain: LibvirtDomainCreatingValidator

    name: str = Field(title='虚拟机名称', example='VM')
    status: VirtualMachineInstance.STAUS = Field(default=1, description='1-关机；2-开机；')

    x: int = Field(example=100, ge=0)
    y: int = Field(example=100, ge=0)


class VirtualMachineInstancePartialUpdatingValidator(BaseValidator):
    name: str = Field(title='虚拟机名称', default=None)
    memory: int = 20000
    cpu: int = 1
    arch: str = "x86_64"  # 系统框架

    x: int = Field(example=100, ge=0)
    y: int = Field(example=100, ge=0)

class VirtualMachineInstantiatingValidator(BaseValidator):
    virtual_machine_template_id: int = Field(title='虚拟机模板id', example=0)
    node_id: int = Field(title='节点id', example=0)

    name: str = Field(title='虚拟机名称', example='VM')
    status: VirtualMachineInstance.STAUS = Field(default=1, description='1-关机；2-开机；')

    x: int = Field(example=100, ge=0)
    y: int = Field(example=100, ge=0)