from pydantic import Field

from core.validators._base import BaseValidator
from eve.models.virtual_machine_network import LibvirtNetwork


class LibvirtNetworkCreatingValidator(BaseValidator):
    # name: str = None
    forward_mode_type: LibvirtNetwork.FORWARD_MODE_TYPE = Field(1, description='1-nat; ')
    # bridge_name: str = None
    bridge_delay: int = 0
    # mac_address: str = None
    ip_address: str = Field(title='IP地址', example='192.168.60.1')
    ip_netmask: str = Field(title='IP掩码', example='255.255.255.0')
    # ip_dhcp_range_start: str = None
    # ip_dhcp_range_end: str = None


class LibvirtNetworkPartialUpdatingValidator(BaseValidator):
    # name: str
    forward_mode_type: LibvirtNetwork.FORWARD_MODE_TYPE = Field(1, description='1-nat; ')
    # bridge_name: str = None
    bridge_delay: int = Field(default=None, example=0)
    # mac_address: str = None
    ip_address: str = Field(title='IP地址', example='192.168.60.1')
    ip_netmask: str = Field(title='IP掩码', example='255.255.255.0')


class VirtualMachineNetworkCreatingValidator(BaseValidator):
    is_visible: bool = False
    x: int = Field(example='100')
    y: int = Field(example='100')
    libvirt_network: LibvirtNetworkCreatingValidator


class VirtualMachineNetworkPartialUpdatingValidator(BaseValidator):
    is_visible: bool = False
    x: int = None
    y: int = None
    libvirt_network: LibvirtNetworkPartialUpdatingValidator = None
