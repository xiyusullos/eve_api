import typing as tp

from pydantic import Field

from core.validators._base import BaseValidator


class VirtualMachineInterfaceCreatingValidator(BaseValidator):
    # name: str
    # type: int
    virtual_machine_instance_id: int
    # virtual_machine_bridge_id: int = None
    ipv4: str = Field(title='IPv4地址', example='192.168.13.100')
    netmask: str = Field(title='子网掩码地址', example='255.255.255.0')
    # gateway: str = Field(title='网关地址', example='192.168.13.1')


class VirtualMachineInterfacePartialUpdatingValidator(BaseValidator):
    type: int = None
    virtual_machine_instance_id: int = None
    virtual_machine_bridge_id: int = None
    ipv4: str
    netmask: str
    gateway: str
