from time import sleep

import pexpect


def config_ipv4(domain_name, ipv4, netmask='255.255.255.0', interface_name='eth0'):
    pass

    cmd_connect_domain = f'virsh console --force {domain_name}'
    child = pexpect.spawn(cmd_connect_domain)

    print('-' * 80)
    print(child.isalive())

    while True:
        i = child.expect([
            'Escape character is.*',
            '.*login:',
            '[Pp]assword.*',
            '\[.*@.*\]',
            pexpect.EOF, pexpect.TIMEOUT
        ], timeout=2)
        print(i)
        if i == 0:
            child.sendline()
        elif i == 1:
            child.sendline('root')  # username
        elif i == 2:
            child.sendline('root')  # password
        else:
            break

    cmd_config_ip_address = f'ip addr add {ipv4}/{netmask} dev {interface_name}'
    print(cmd_config_ip_address)
    child.sendline(cmd_config_ip_address)
    child.sendline('ip a')

    # # print(child.readline())
    # is_go_on = True
    # while is_go_on:
    #     out = child.read(1).decode('utf-8')
    #     is_go_on = len(out) > 0
    #     print(out, end='.')
    #     # sleep(0.01)
    child.close()