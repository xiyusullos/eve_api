import sys

import pexpect

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print('miss arg: domain_name')
        sys.exit()

    domain_name = sys.argv[1]
    child = pexpect.spawn(f'virsh console --force {domain_name}')
    child.interact()
