from starlette.responses import RedirectResponse

from core.actions._base import CreateAction, DetailAction, SingleAction, CustomizedAction


class CreateVirtualMachineAction(CreateAction):
    name_doc = '创建虚拟机'

    def init_handle(self):
        self.handle = self.repository.create_domain


class InstantiateVirtualMachineAction(CreateAction):
    path = '/instantiate'
    name_doc = '实例化'

    def init_handle(self):
        self.handle = self.repository.instantiate


class BootDomainAction(CustomizedAction):
    path = '/{id}/boot'
    name_doc = "启动虚拟机"

    def init_handle(self):
        self.handle = self.repository.boot_domain


class DestroyDomainAction(DetailAction):
    path = '/{id}/destroy'
    name_doc = "销毁虚拟机"

    def init_handle(self):
        self.handle = self.repository.destroy_domain


class ShutdownDomainAction(CustomizedAction):
    path = '/{id}/shutdown'
    name_doc = "关闭虚拟机"

    def init_handle(self):
        self.handle = self.repository.shutdown_domain


class CheckStatusAction(CreateAction):
    path = '/{name}'
    name_doc = "查看运行状态"

    def init_handle(self):
        self.handle = self.repository.check_status


class DetailConsoleAction(DetailAction):
    path = '/{id}/console'
    name_doc = "进入虚拟机控制台"

    def init_handle(self):
        self.handle = self.repository.enter_console
