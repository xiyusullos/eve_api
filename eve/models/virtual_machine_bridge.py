# coding: utf-8
from sqlalchemy import Column, DateTime, String, BIGINT, text
from sqlalchemy.dialects.mysql import INTEGER

from core.models._base import BaseModel, TimestampMixin


class VirtualMachineBridge(BaseModel, TimestampMixin):
    __tablename__ = 'virtual_machine_bridge'

    id = Column(INTEGER(20), primary_key=True)
    name = Column(String(20), comment='简写名称')
    is_visible = Column(INTEGER(1), comment='连线是否可见')
    virtual_machine_interface_source_id = Column(INTEGER(20), comment='连线起点')
    virtual_machine_interface_target_id = Column(INTEGER(20), comment='连线终点')
