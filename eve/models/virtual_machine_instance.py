# coding: utf-8
from enum import Enum, IntEnum

import libvirt
from orator import mutator
from orator.orm import belongs_to
from sqlalchemy import Column, DateTime, String, BIGINT, text, Integer, ForeignKey
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.orm import relationship

from eve.models._base import BaseModel, SoftDeletes


class LibvirtDomain(BaseModel, SoftDeletes):
    __table__ = 'libvirt_domain'

    class DOMAIN_TYPE(IntEnum):  # key must be lower
        kvm = 1
        xen = 2
        qemu = 3
        lxc = 4

    class OS_ARCH_TYPE(IntEnum):
        x86_64 = 1

    @mutator
    def os_arch_type(self, v: IntEnum):
        self.set_raw_attribute('os_arch_type', v.value)


class VirtualMachineInstance(BaseModel, SoftDeletes):
    __table__ = 'virtual_machine_instance'
    __fillable__ = ['virtual_machine_template_id', 'node_id', 'libvirt_domain_id',
                    'name', 'domain_name', 'x', 'y', 'status', 'cpu', 'ram']

    class STAUS(IntEnum):
        powered_off = 1
        powered_on = 2

    @belongs_to
    def libvirt_domain(self):
        return LibvirtDomain

    @mutator
    def status(self, value: STAUS):
        self.set_raw_attribute('status', value.value)

# class LibvirtDomain(BaseModel, IntegerIdMixin, TimestampMixin):
#     class DOMAIN_TYPE(IntEnum):  # key must be lower
#         kvm = 1
#         xen = 2
#         qemu = 3
#         lxc = 4
#
#     class OS_ARCH_TYPE(IntEnum):
#         x86_64 = 1
#
#     __tablename__ = 'libvirt_domain'
#
#     # domain_type = Column(INTEGER(20))  # kvm
#     name = Column(String(36), comment='libvirt的domain name')
#     memory = Column(INTEGER(11), comment='内存大小。单位：MB')
#     cpu_num = Column(INTEGER(11), comment='CPU核数')
#     os_arch_type = Column(INTEGER(8), comment='OS架构类型')
#
#
# class VirtualMachineInstance(BaseModel, IntegerIdMixin, TimestampMixin):
#     class STAUS(IntEnum):
#         powered_off = 1
#         powered_on = 2
#
#     __tablename__ = 'virtual_machine_instance'
#
#     virtual_machine_template_id = Column(INTEGER(20), comment='虚拟机模板id')
#
#     name = Column(String(25), comment='简写名称')
#     description = Column(String(25), comment='描述 厂商以及全称')
#
#     libvirt_domain_id = Column(INTEGER(20), ForeignKey('libvirt_domain.id'), nullable=False)
#     libvirt_domain = relationship('LibvirtDomain', lazy='immediate')
#
#     x = Column(INTEGER(25), comment='前端显示的位置x')
#     y = Column(INTEGER(25), comment='前端显示的位置y')
#     delay = Column(INTEGER(25), comment='延迟，忽略')
#     config = Column(String(25), comment='配置，忽略')
#
#     status = Column(INTEGER(4), comment='虚拟机实例状态')
