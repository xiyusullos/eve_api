from enum import IntEnum

from orator import SoftDeletes
from orator.orm import belongs_to

from eve.models._base import BaseModel


class LibvirtNetwork(BaseModel, SoftDeletes):
    __table__ = 'libvirt_network'

    class FORWARD_MODE_TYPE(IntEnum):
        nat = 1


class VirtualMachineNetwork(BaseModel, SoftDeletes):
    __table__ = 'virtual_machine_network'
    __fillable__ = ['libvirt_network_id', 'is_visible', 'x', 'y']

    @belongs_to
    def libvirt_network(self):
        return LibvirtNetwork
