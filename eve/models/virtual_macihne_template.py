# coding: utf-8
from orator import SoftDeletes

from eve.models._base import BaseModel


class VirtualMachineTemplate(BaseModel, SoftDeletes):
    __table__ = 'virtual_machine_template'
    __fillable__ = ['id', 'name', 'cpu', 'ram', 'qcow2']

# class VirtualMachineTemplate(BaseModel):
#     __tablename__ = 'virtual_machine_template'
#
#     id = Column(BIGINT, primary_key=True, autoincrement=True)
#     yml = Column(String(25), comment='yml文件名，唯一')
#     name = Column(String(25), comment='简写名称')
#     is_existed = Column(TINYINT, server_default=text("'1'"), comment='是否存在，默认为1')
#     type = Column(String(255), comment='镜像格式')
#     description = Column(String(255), comment='描述 厂商以及全称')
#     is_cpu_limited = Column(TINYINT, server_default=text("'1'"), comment='是否限制CPU，默认为1')
#     icon = Column(String(255), comment='图标')
#     cpu = Column(INTEGER(11), comment='分配CPU数量')
#     ram = Column(INTEGER(11), comment='分配运存')
#     ethernet = Column(INTEGER(11), comment='以太网帧格式')
#     console = Column(String(255), comment='原生编译方式，忽略')
#     qemu_arch = Column(String(255), comment='CPU架构')
#     qemu_options = Column(String(1000), comment='可选项')
