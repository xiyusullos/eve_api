import arrow
from orator import Model, accessor
from orator import SoftDeletes as OratorSoftDeletes

from eve.models.observers import BaseObserver


class BaseModel(Model):
    __guarded__ = ['id']

    @classmethod
    def _boot(cls):
        cls.observe(BaseObserver())

        super()._boot()

    # def get_dates(self):
    #     return []

    # @accessor
    # def created_at(self):
    #     t = self.get_raw_attribute(self.get_created_at_column())
    #     print(t)
    #     return arrow.get(t).to('Asia/Shanghai').format('YYYY-MM-DD HH:mm:ss')


class SoftDeletes(OratorSoftDeletes):
    pass
