from core.models._base import BaseModel
from sqlalchemy import CHAR, Column, DECIMAL, DateTime, Enum, Index, LargeBinary, String, TIMESTAMP, Text, text, INTEGER


class Domain(BaseModel):
    __tablename__ = 'domain'
    id = Column(INTEGER, primary_key=True, nullable=False)
    left = Column(INTEGER)
    top = Column(INTEGER)
    path = Column(String(255))
    name = Column(String(255))


class DomainMapper(BaseModel):
    __tablename__ = 'domain_mapper'
    id = Column(INTEGER, primary_key=True, nullable=False)
    original_name = Column(String(255))
    mapping_name = Column(String(255))
