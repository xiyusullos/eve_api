# coding: utf-8
from sqlalchemy import Column, DateTime, String, BIGINT, text
from sqlalchemy.dialects.mysql import INTEGER

from core.models._base import BaseModel, TimestampMixin


class VirtualMachineInterface(BaseModel, TimestampMixin):
    __tablename__ = 'virtual_machine_interface'

    id = Column(INTEGER(25), primary_key=True)
    name = Column(String(25), comment='连线名称')
    type = Column(INTEGER(25), comment='类型')
    virtual_machine_instance_id = Column(INTEGER(25), comment='所属设备')
    virtual_machine_bridge_id = Column(INTEGER(25), comment='连线id，默认')
    ipv4 = Column(String(255), comment='ip地址')
    netmask = Column(String(255), comment='子网掩码')
    gateway = Column(String(255), comment='网关')
