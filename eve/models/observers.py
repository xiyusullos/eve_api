import arrow


class BaseObserver(object):
    def creating(self, model):
        model.created_at = arrow.now('Asia/Shanghai').datetime
        model.updated_at = arrow.now('Asia/Shanghai').datetime
        model.created_by = 0
        model.updated_by = 0

    def updating(self, model):
        model.updated_at = arrow.now('Asia/Shanghai').datetime
        model.updated_by = 0

    def saving(self, model):
        pass

    def saved(self, model):
        pass
