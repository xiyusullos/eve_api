from core.exceptions._all import MyHTTPException


class VirtualMachineException(MyHTTPException):
    pass
