from core.transformers._base import OrmTransformer, BaseTransformer, OrmTransformerWithTimestamp


class VirtualMachineInterface(OrmTransformerWithTimestamp):
    name: str
    type: int
    virtual_machine_instance_id: int
    virtual_machine_bridge_id: int = None
    ipv4: str
    netmask: str
    gateway: str
