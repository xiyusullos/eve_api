from core.transformers._base import OrmTransformer, BaseTransformer, OrmTransformerWithTimestamp


class LibvirtNetwork(OrmTransformer):
    name: str
    uuid: str
    forward_mode_type: int = 1
    bridge_name: str
    bridge_stp: str = "on"
    bridge_delay: str = "0"
    mac_address: str
    ip_address: str
    ip_netmask: str
    # ip_dhcp_range_start: str = "192.168.142.2"
    # ip_dhcp_range_end: str = "192.168.142.254"


class VirtualMachineNetworkDetail(OrmTransformer):
    is_visible: bool
    x: int
    y: int
    libvirt_network: LibvirtNetwork


class VirtualMachineNetworkSummary(OrmTransformer):
    is_visible: bool
    x: int
    y: int
