from datetime import datetime

from core.transformers._base import OrmTransformer, BaseTransformer


class VirtualMachineTemplateDetail(OrmTransformer):
    name: str
    is_existed: int
    type: str = None
    description: str = None
    is_cpu_limited: int = None
    icon: str = None
    cpu: int = None
    ram: int = None
    ethernet: int = None
    console: str = None
    qemu_arch: str = None
    qemu_options: str = None


class VirtualMachineTemplateSummary(OrmTransformer):
    name: str
    description: str = None
