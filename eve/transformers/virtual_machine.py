from typing import Optional

from core.transformers._base import OrmTransformer, BaseTransformer


class VirtualMachine(BaseTransformer):
    flag: int
