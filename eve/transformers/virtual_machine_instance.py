from pydantic import Field

from core.transformers._base import OrmTransformerWithTimestamp
from eve.models.virtual_machine_instance import VirtualMachineInstance


class LibvirtDomainDetail(OrmTransformerWithTimestamp):
    name: str
    memory: int
    cpu_num: int
    os_arch_type: int


class VirtualMachineInstanceDetail(OrmTransformerWithTimestamp):
    name: str
    description: str = None
    status: VirtualMachineInstance.STAUS = Field(description='1-关机；2-开机；')
    x: int
    y: int
    delay: int = None
    config: str = None
    libvirt_domain: LibvirtDomainDetail


class VirtualMachineInstanceSummary(OrmTransformerWithTimestamp):
    name: str
    description: str = None
